﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AnimalProject
{
   public class Animal
    {
        public int Id { get; set; }
        public string Name { get; set; } = "Animal";
        public string Description { get; set; }
        // public override string ToString()
        // {
        //    return Id + " " + FirstName + " " + LastName + " ";
        // }

        public override string ToString() => $"Animal Info : {Id } {Name } : {Description}";

    }
}
