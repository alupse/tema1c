﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AnimalProject
{
    public class AnimalController
    {
        readonly IAnimalRepository _animalRepository;
        public AnimalController(IAnimalRepository animalRepository)
        {
            _animalRepository = animalRepository;
        }
        public AnimalController()
        {

        }
        public void Insert(Animal a)
        {
            _animalRepository.Add(a);
        }
        public void Remove(Animal a) => _animalRepository.Delete(a);
        public Animal GetById(int id)
        {
            return _animalRepository.GetById(id);
        }

        public void ChangeDescription(Animal a, String description)
        {
            _animalRepository.ModifyDescription(a, description);
        }
        public IEnumerable<Animal> GetAll() => _animalRepository.GetAll();
    }
}
