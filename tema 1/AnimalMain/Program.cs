﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AnimalProject;

namespace AnimalMain
{
    class Program
    {
        static void Main(string[] args)
        {
           Animal a1 = new Animal();
            a1.Id = 1;
            a1.Description = "Cute little kitty";
           Animal a2 = new Tiger
            {
                
                Description = "Baby little tiger",
                Id = 2
            };
            Animal a3 = new Tiger
            {

                Description = "Old tiger",
                Id = 3
            };
            Animal a4 = new Animal
            {

                Description = "Puppy looking for a new home",
                Id = 4
            };

            AnimalRepository animalRepository = new AnimalRepository();
            animalRepository.Add(a1);
            animalRepository.Add(a2);
            animalRepository.Add(a3);
            animalRepository.Add(a4);
            animalRepository.GetById(1);
            IEnumerable<Animal> animals = animalRepository.GetAll();

            Animal first = animalRepository.GetById(2);
            Animal second = animalRepository.GetById(4);

              Console.WriteLine(first);
             Console.WriteLine(second);
            animalRepository.Delete(a3);
            Console.WriteLine("==================");
            foreach (Animal p in animals)
            {
                Console.WriteLine(p);
            }
            Console.WriteLine("==================");
            animalRepository.ModifyDescription(a1, "BAD KITTY");
            Animal third = animalRepository.GetById(1);
            Console.WriteLine(third);

        }
    }
}
