﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using AnimalProject;
using System.Collections.Generic;
using System.Linq;

namespace AnimalTest
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void GetData_Test()
        {
            Mock<IAnimalRepository> repoMock = new Mock<IAnimalRepository>();
            Animal a1 = new Animal
            {
                
                Description = "Brown furry pet",
                Id = 5
            };
            repoMock.Setup(x => x.Add(a1));

            repoMock.Setup(x => x.GetById(2)).Returns(a1);

            AnimalController controller = new AnimalController(repoMock.Object);
            Animal result = controller.GetById(2);

            Assert.AreEqual(a1.Id, result.Id);
            Assert.AreEqual(a1.Name, result.Name);
            Assert.AreEqual(a1.Description, result.Description);



        }
        [TestMethod]
        public void GetAllData_Test()
        {
            Mock<IAnimalRepository> mock = new Mock<IAnimalRepository>();
           Animal a1 = new Animal
            {
                Description = "Sweet little bunny",
                Id = 1
            };
            Animal a2 = new Tiger
            {
                
                Description = "Tiger mom lookin for a zoo",
                Id = 2
            };
            List<Animal> animals = new List<Animal>
            { a1,
              a2
            };
            mock.Setup(x => x.GetAll()).Returns(animals);
            AnimalController controller = new AnimalController(mock.Object);
            List<Animal> result = controller.GetAll().ToList();
            Assert.AreEqual(2, result.Count);

        }

        [TestMethod]
        public void Delete_Test()
        {
            Mock<IAnimalRepository> mock = new Mock<IAnimalRepository>();
            Animal a1 = new Animal
            {
                Description = "Sweet little bunny",
                Id = 1
            };
            Animal a2 = new Tiger
            {

                Description = "Tiger mom lookin for a zoo",
                Id = 2
            };

          
            AnimalRepository animalRepository = new AnimalRepository();
            animalRepository.Add(a1);
            animalRepository.Add(a2);
            animalRepository.Delete(a2);
            IEnumerable<Animal> animals= animalRepository.GetAll();
            Animal first = animalRepository.GetById(1);
            Assert.AreEqual(1,animals.Count());
            Assert.AreEqual(a1.Id, first.Id);
        }

        

        [TestMethod]
        public void ModifyDescription_Test()
        {
            Mock<IAnimalRepository> mock = new Mock<IAnimalRepository>();
            Animal a1 = new Animal
            {
                Description = "Sweet little bunny",
                Id = 1
            };
            Animal a2 = new Tiger
            {

                Description = "Tiger mom lookin for a zoo",
                Id = 2
            };


            AnimalRepository animalRepository = new AnimalRepository();
            animalRepository.Add(a1);
            animalRepository.Add(a2);
            animalRepository.ModifyDescription(a2,"Tiger dad single and lonley");
            IEnumerable<Animal> animals = animalRepository.GetAll();
            Animal first = animalRepository.GetById(2);
            Assert.AreEqual(a2.Description, first.Description);
        }


    }
}
